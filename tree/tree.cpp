#include <iostream>
 
using namespace std;
 
class Node
{
public:
    int key;
    Node * left;
    Node * right;
    Node () : key (0), left (nullptr), right (nullptr) { }
};
 
Node* find (Node* start, int value)
// vysledek : ukazatel na nalezeny prvek nebo nullptr
{
    while (start != nullptr && start->key != value)
    {
        if (value < start->key) 
            start = start->left;
        else if (value > start->key) 
            start = start->right;
    }
    return start;
}
 
Node* search(Node* start, int value)
// vysledek : ukazatel na nalezeny prvek nebo nullptr
{
    if (start == nullptr)
        return nullptr;
    else if (start->key == value)
        return start;
    else if (value < start->key)
        return search(start->left, value);
    else
        return search(start->right, value);
}
 
void insert (Node * & start, int value)
{
    if (start == nullptr)
    {
        start = new Node;
        start->key = value;
        // start->left = nullptr;
        // start->right = nullptr;
    }
    else if (start->key == value)
    {  }
    else if (value < start->key)
        insert (start->left, value);
    else
        insert (start->right, value);
}
 
void print(Node* start, int level = 1)
{
    if (start != nullptr)
    {
        for (int i = 1; i <= level; i++) cout << "-";
        cout << start->key << endl;
        print(start->left, level + 1);
        print(start->right, level + 1);
    }
}
 
Node* root = nullptr;
 
int main()
{
    insert(root, 10);
    insert(root, 5);
    insert(root, 20);
    insert(root, 2);
    insert(root, 7);
    insert(root, 15);
    insert(root, 30);
    print(root);
    Node* answer = find (root, 7);
    if (answer != nullptr) cout << "Nasel jsem ... ..." << endl;
}
 
/*
inline void swap (int & a, int & b)
{
    int t = a;
    a = b;
    b = t;
}
 
void oldswap (int * a, int * b)
{
    int t = *a;
    *a = *b;
    *b = t;
}
*/
 
/*
// swap (10, 20);
int x = 100;
int y = 200;
swap(x, y);
oldswap(&x, &y);
*/

 
