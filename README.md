# zalg

Základy algoritmizace - cvičení
===============================

Nejjednodušší přístup k zdrojovým souborům je tlačítko “Download”, schované mezi tlačítky “Find file” a “Clone”. 

Windows Git: http://git-scm.com/download/win

a užitečný doplněk Windows Tortoise Git: http://tortoisegit.org/download/

Linux: git clone http://gitlab.fjfi.cvut.cz/culikzde/zalg.git

A později v adresáři zalg stažení nových verzí: git pull


[tree](tree)    ... Binární strom

[dama](dama)    ... Problém osmi dam

[dama-qt](dama-qt) ... Problém osmi dam s grafickým výstupem