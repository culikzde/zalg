#ifndef MOJEOKNO_H
#define MOJEOKNO_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QTreeWidgetItem>

namespace Ui {
class Dama;
}

class MyItem;

const int N = 8;

class Dama : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Dama(QWidget *parent = 0);
    ~Dama();

private:
    int pocet;
    int pole [8];
    QTreeWidgetItem * top;

    void zobrazPolicko (int i, int k);
    void vymazPolicko (int i, int k);
    void ulozVysledek ();
    MyItem * zobrazStav (int i, int k);
    void obarviVetev (bool ok);

    void umisti (int k);
    
private slots:
    void on_horizontalSlider_sliderMoved(int position);
    void on_actionRun_triggered();

    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

private:
    Ui::Dama *ui;
};

class MyItem : public QTreeWidgetItem // uzel stromu
{
   public:
       MyItem () : QTreeWidgetItem () { }
       QTableWidgetItem * pole [N][N]; // pamatuje si figurky
};

#endif // MOJEOKNO_H
