#include "dama.h"
#include "ui_dama.h"
#include <cassert>

QString sloupec (int x)
{
    return QString ('a' + x);
}

QString radka (int y)
{
    return QString::number (N - y);
}

QString popis (int x, int y)
{
    return sloupec (x) + radka (y);
}

void setupTable (QTableWidget * table)
{
    table->setColumnCount (N);
    table->setRowCount (N);

    int h = table->rowHeight (0);
    for (int i = 0; i < N; i++)
        table->setColumnWidth (i, h);

    QStringList list;
    for (int i = 0; i < N; i++)
        list << sloupec (i);
    table->setHorizontalHeaderLabels(list);

    list.clear();
    for (int i = 0; i < N; i++)
        list << radka (i);
    table->setVerticalHeaderLabels(list);
}

Dama::Dama(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Dama)
{
    ui->setupUi(this);

    ui->splitter->setStretchFactor (0, 1);
    ui->splitter->setStretchFactor (1, 1);
    ui->splitter->setStretchFactor (2, 2);

    setupTable (ui->tableWidget);
}

Dama::~Dama()
{
    delete ui;
}

void Dama::zobrazPolicko (int i, int k)
{
    QString s = QString::number (k);
    QColor c = QColor::fromHsv((k*30) % 256, 200, 255);

    QTableWidgetItem * t = new QTableWidgetItem;
    t->setText (s);
    t->setTextColor(QColor ("red"));
    t->setBackgroundColor(c);
    ui->tableWidget->setItem(i, k, t);
}

void Dama::vymazPolicko(int i, int k)
{
     QTableWidgetItem * t = ui->tableWidget->item (i,k);
     delete t;
}

MyItem * Dama::zobrazStav (int i, int k)
{
    // vytvor uzel ve stromu
    MyItem * q = new MyItem;
    q->setText (0, popis (k, i));
    if (top != nullptr)
       top->addChild (q);
    else
       ui->treeWidget->addTopLevelItem (q);

    // do stromu uloz soucasny stav sachovnice
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
        {
            QTableWidgetItem * u = ui->tableWidget->item (j,i);
            if (u != nullptr)
                u = u->clone();
            else
                u = new QTableWidgetItem ();
            q->pole[i][j] = u;
        }

    return q; // vysledkem je vytvoreny uzel ve stromu
}

void Dama::obarviVetev (bool ok)
{
    if (top != nullptr)
       if (ok)
           top->setForeground (0, QColor ("blue"));
        else
           top->setForeground (0, QColor ("red"));
}

void Dama::ulozVysledek ()
{
    QTableWidget * t = new QTableWidget;
    setupTable (t);

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
        {
            QTableWidgetItem * u = ui->tableWidget->item (j, i);
            if (u != nullptr)
                t->setItem (j, i, u->clone ());
        }

    pocet ++;
    ui->tabWidget->addTab (t, QString::number(pocet));
}

void Dama::umisti (int k)
{
    for (int i = 0; i < N; i ++)
    {
        bool ok = true;
        for (int j = 0; j < k ; j++)
            if (pole [j] == i || pole [j] == i-(k-j) || pole [j] == i+(k-j))
                ok = false;

        if (ok)
        {
            pole [k] = i;

            QTreeWidgetItem * save = top; // uloz soucasnou pozici ve stromu
            int puvodni_pocet = pocet;
            zobrazPolicko (i, k);
            top = zobrazStav (i, k); // vytvor ve stromu novy uzel

            if (k < N-1)
                umisti (k+1);
            else
                ulozVysledek();

            vymazPolicko (i, k);
            obarviVetev (pocet != puvodni_pocet);
            top = save; // vrat se ve stromu
        }
    }
}

void Dama::on_actionRun_triggered()
{
    ui->tableWidget->clearContents ();

    ui->treeWidget->clear ();

    ui->tabWidget->clear ();

    top = nullptr;

    for (int i = 0; i < N; i++)
        pole [i] = 0;

    pocet = 0;

    umisti (0);

    ui->horizontalSlider->setMinimum (1);
    ui->horizontalSlider->setMaximum (pocet);
}

void Dama::on_horizontalSlider_sliderMoved(int position)
{
    ui->tabWidget->setCurrentIndex (position-1);
}

void Dama::on_treeWidget_itemClicked (QTreeWidgetItem * treeItem, int column)
{
    MyItem * item = dynamic_cast <MyItem*> (treeItem);
    if (item != nullptr)
    {
        ui->tableWidget->clearContents();
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                ui->tableWidget->setItem (j, i, item->pole [i][j]->clone());
    }

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Dama w;
    w.show();

    return a.exec();
}

