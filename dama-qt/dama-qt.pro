#-------------------------------------------------
#
# Project created by QtCreator 2013-04-10T11:43:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tabulka
TEMPLATE = app


SOURCES += \
    dama.cc

HEADERS  += \
    dama.h

FORMS    += \
    dama.ui
