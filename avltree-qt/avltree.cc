#include "avltree.h"
#include "ui_avltree.h"
#include <QApplication>
#include <QStyleFactory>

struct Item
{
    int key;
    int height;
    Item * left;
    Item * right;
    Item () : key (0), height(1), left (nullptr), right (nullptr) { }
};

inline int h (Item * p)
{
    if (p == nullptr)
        return 0;
    else
        return p->height;
}

void update (Item * p)
{
    int L = h (p->left);
    int R = h (p->right);
    if (L > R)
        p->height = L + 1;
    else
        p->height = R + 1;
}

void enter (Item * & root, int val)
{
    if (root == nullptr)
    {
        root = new Item;
        root->key = val;
    }
    else if (val < root->key) enter (root->left, val);
    else if (val > root->key) enter (root->right, val);
    else { }

    update (root);
    int L = h(root->left);
    int R = h(root->right);
    Item * a = root;
    if (L > R + 1)
    {
        Item * b = a->left;
        int LL = h(b->left);
        int RR = h(b->right);
        if (LL > RR)
        {
            Item * c = b->left;
            //     a
            //   b
            // c
            Item * t = b->right; // podstrom
            root = b; // novy koren
            //     b
            //  c     a
            b->right = a;
            a->left = t;
            update (a);
            update (b); // jako posledni
        }
        else
        {
            Item * c = b->right;
            //         a
            //   b
            //      c
            Item * t = c->left; // podstrom
            Item * u = c->right; // podstrom
            root = c; // novy koren
            //     c
            //  b     a
            c->left = b;
            c->right = a;
            b->right = t;
            a->left = u;
            update(a);
            update(b);
            update(c); // jako posledni
        }
    }
    else if (R > L + 1)
    {
        Item * b = a->right;
        int LL = h(b->right);
        int RR = h(b->left);
        if (LL > RR)
        {
            Item * c = b->right;
            //  a
            //     b
            //        c
            Item * t = b->left; // podstrom
            root = b; // novy koren
            //     b
            //  a     c
            b->left = a;
            a->right = t;
            update(a);
            update(b); // jako posledni
        }
        else
        {
            Item * c = b->left;
            //  a
            //        b
            //     c
            Item * t = c->right; // podstrom
            Item * u = c->left; // podstrom
            root = c; // novy koren
            //     c
            //  a     b
            c->right = b;
            c->left = a;
            b->left = t;
            a->right = u;
            update(a);
            update(b);
            update(c); // jako posledni
        }
    }
}

void MainWindow::displayBranch (QTreeWidgetItem * target, Item * branch)
{
    if (branch != nullptr)
    {
        QTreeWidgetItem * node = new QTreeWidgetItem;
        if (target == nullptr)
            ui->treeWidget->addTopLevelItem (node);
        else
            target->addChild (node);

        node->setText (0, QString::number (branch->key));
        node->setToolTip (0, QString::number (branch->height));

        displayBranch (node, branch->left);
        displayBranch (node, branch->right);
    }
}


void MainWindow::on_action_Run_triggered()
{
   Item * root = nullptr;

   const int N = 32;
   // const int N = 16*1024;

   for (int k = 1; k <= N; k++)
      enter (root, k);

   ui->treeWidget->clear ();
   displayBranch (nullptr, root);
   ui->treeWidget->expandAll ();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setStyle(QStyleFactory::create("Windows"));
    MainWindow w;
    w.show();

    return a.exec();
}

