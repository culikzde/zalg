#ifndef AVLTREE_H
#define AVLTREE_H

#include <QMainWindow>
#include <QTreeWidget>

namespace Ui {
class MainWindow;
}

class Item;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_action_Run_triggered();

private:
    Ui::MainWindow *ui;
    void displayBranch (QTreeWidgetItem *target, Item *branch);
};

#endif // AVLTREE_H
