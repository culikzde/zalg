
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = btreewin

TEMPLATE = app

SOURCES += btreewin.cc

HEADERS  += btreewin.h

FORMS    += btreewin.ui

RESOURCES +=  btreewin.qrc

