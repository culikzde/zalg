#ifndef BTREEWIN_H
#define BTREEWIN_H

#include <QMainWindow>




namespace Ui {
class BTreeWin;
}

class BTreeWin : public QMainWindow
{
    Q_OBJECT

public:
    explicit BTreeWin(QWidget *parent = 0);
    ~BTreeWin();

public:
    Ui::BTreeWin *ui;
};

#endif // BTREEWIN_H
