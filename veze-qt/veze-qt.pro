
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = veze
TEMPLATE = app


SOURCES += tower.cc

HEADERS  += tower.h

FORMS    += tower.ui
