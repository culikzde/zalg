#include "tower.h"
#include "ui_tower.h"
#include <QApplication>

#include <QGraphicsRectItem>

#include <string>
#include <iostream>
#include <cassert>
using namespace std;

void uloz (); // predbezna deklarace - zaznamenej stav vezi

const int Max = 20;
int n = 10;

struct Vez
{
    int v;
    int p [Max];
};

Vez A, B, C;

void nuluj (Vez & X)
{
    X.v=0;
    for(int i=0; i<Max; i++)
        X.p[i]=0;
}

void init (Vez & X)
{
    X.v=n;
    for (int i=0; i<Max; i++)
        X.p[i]=n-i;
}

void krok (Vez & X, Vez & Y){
    int h;
    h=X.p[X.v-1];
    X.p[X.v-1]=0;
    X.v=X.v-1;

    Y.v=Y.v+1;
    Y.p[Y.v-1]=h;
}

void hraj (Vez & X, Vez & Y, Vez & Z, int k)
{
    if (k>1) hraj (X, Z, Y, k-1);
    krok (X, Z);
    uloz ();
    if (k>1) hraj (Y, X, Z, k-1);
}

void run ()
{
    init(A);
    nuluj(B);
    nuluj(C);
    uloz ();
    hraj (A, B, C, n);
}

/* ----------------------------------------------------------------------- */

void zobraz (Vez & X)
{
    for (int i = 0; i < X.v; i++)
        cout << X.p[i];
    for (int i = X.v; i <= n; i++)
        cout << ' ';
}

void tiskni (Vez & X, Vez & Y, Vez & Z)
{
    zobraz (X);
    cout << " : ";
    zobraz (Y);
    cout << " : ";
    zobraz (Z);
    cout << endl;
}

/* ----------------------------------------------------------------------- */

struct TriVeze
{
    Vez AA;
    Vez BB;
    Vez CC;
};

QList <TriVeze> zaznam;

void uloz ()
{
    TriVeze t;
    t.AA = A;
    t.BB = B;
    t.CC = C;
    zaznam.append (t);
}

/* ----------------------------------------------------------------------- */

TowerWindow::TowerWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Tower)
{
    ui->setupUi(this);

    QGraphicsScene * scene = new QGraphicsScene;
    ui->graphicsView->setScene (scene);

    ui->spinBox->setMinimum (1);
    ui->spinBox->setMaximum (Max);
    ui->spinBox->setValue (n);
}

TowerWindow::~TowerWindow()
{
    delete ui;
}

QColor color (int i, int saturation)
{
    return QColor::fromHsv ((i*360/n) % 360, saturation, 240);
}

void TowerWindow::nakresliVez (int relx, const Vez & U)
{
    int h = ui->graphicsView->height();
    int w = ui->graphicsView->width();

    int xpos = w / 4 * (2+relx);

    int xstep = w / (4 * (n+2));
    int ystep = h / (n+2);
    int ysize = ystep * 9 / 10;

    for (int i = 0; i < U.v; i++)
    {
        int ypos = h - (i+1) * ystep;
        int xsize = xstep * U.p[i];

        QGraphicsRectItem * r = new QGraphicsRectItem;
        r->setRect (xpos-xsize/2, ypos, xsize, ysize);
        r->setPen (color (U.p[i], 200));
        r->setBrush (color (U.p[i], 120));
        ui->graphicsView->scene()->addItem (r);
    }
}

void TowerWindow::nakresli (int k)
{
    ui->graphicsView->scene()->clear();
    if (k >= 0 && k < zaznam.count ())
    {
       TriVeze t = zaznam [k];
       nakresliVez (-1, t.AA);
       nakresliVez ( 0, t.BB);
       nakresliVez ( 1, t.CC);
    }
}

void TowerWindow::on_actionRun_triggered()
{
   n = ui->spinBox->value();
   assert (n > 0 && n <= Max);
   zaznam.clear (); // vymaz predchozi zaznamy
   run ();
   ui->horizontalScrollBar->setMaximum (zaznam.count() - 1); // indexy od 0
   ui->horizontalScrollBar->setValue (zaznam.count() - 1);
   ui->lineEdit->setText (QString::number(zaznam.count()));
}

void TowerWindow::on_horizontalScrollBar_valueChanged (int value)
{
   nakresli (value);
}

void TowerWindow::resizeEvent (QResizeEvent * event)
{
    nakresli (ui->horizontalScrollBar->value());
}

int main (int argc, char *argv[])
{
    QApplication a(argc, argv);
    TowerWindow w;
    w.show();

    return a.exec();
}
