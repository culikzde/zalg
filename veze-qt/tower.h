#ifndef TOWER_H
#define TOWER_H

#include <QMainWindow>

namespace Ui {
class Tower;
}

struct Vez;

class TowerWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TowerWindow(QWidget *parent = 0);
    ~TowerWindow();
    void resizeEvent (QResizeEvent * event);

private:
    void nakresliVez (int relx, const Vez & U);
    void nakresli (int k);

private slots:
    void on_actionRun_triggered();

    void on_horizontalScrollBar_valueChanged(int value);

private:
    Ui::Tower *ui;
};

#endif // TOWER_H
