#include "jezdec.h"
#include "ui_jezdec.h"
#include <cassert>

QString sloupec (int x)
{
    return QString ('a' + x);
}

QString radka (int y)
{
    return QString::number (N - y);
}

QString popis (int x, int y)
{
    return sloupec (x) + radka (y);
}

void setupTable (QTableWidget * table)
{
    table->setColumnCount (N);
    table->setRowCount (N);

    int h = table->rowHeight (0);
    for (int i = 0; i < N; i++)
        table->setColumnWidth (i, h);

    QStringList list;
    for (int i = 0; i < N; i++)
        list << sloupec (i);
    table->setHorizontalHeaderLabels(list);

    list.clear();
    for (int i = 0; i < N; i++)
        list << radka (i);
    table->setVerticalHeaderLabels(list);
}

Jezdec::Jezdec(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Jezdec)
{
    ui->setupUi(this);

    ui->spinBoxX->setMaximum (N-1);
    ui->spinBoxY->setMaximum (N-1);
    ui->horizontalSlider->setMaximum (2*N);

    setupTable (ui->tableWidget);
    setupTable (ui->traceTable);
}

Jezdec::~Jezdec()
{
    delete ui;
}

MyItem * Jezdec::zobrazStav (int x, int y, int v, int x0, int y0)
{
    // popisy a barva
    QString s = QString::number (v);
    QString t;
    if (x0 < 0 || y0 < 0)
       t = "";
    else
       t = popis (x0, y0);
    QColor c = QColor::fromHsv((v*20) % 256, 200, 255);

    // nove policko na sachovnici
    QTableWidgetItem * p = new QTableWidgetItem;
    p->setText (s);
    p->setToolTip (t);
    p->setBackgroundColor (c);
    p->setTextColor(QColor ("red"));
    ui->tableWidget->setItem (y, x, p);

    // vytvor uzel ve stromu
    MyItem * q = new MyItem;
    if (t != "")
       t = t + " -> ";
    q->setText (0, QString::number (v) + " : " + t  + popis (x, y));
    if (top != nullptr)
       top->addChild (q);
    else
       ui->treeWidget->addTopLevelItem (q);

    // do stromu uloz soucasny stav sachovnice
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
        {
            QTableWidgetItem * u = ui->tableWidget->item (j,i);
            if (u != nullptr)
                u = u->clone();
            else
                u = new QTableWidgetItem ();
            q->pole[i][j] = u;
        }

    return q; // vysledkem je vytvoreny uzel ve stromu
}

void Jezdec::jump (int i, int k, int v, int i0, int k0)
{
    if (i >= 0 && i < N && k >= 0 && k < N && v <= limit)
    {
        if (p[i][k] == -1 || p[i][k] > v && ! udelej_chybu)
        {
            QTreeWidgetItem * save = top; // uloz soucasnou pozici ve stromu
            MyItem * q = zobrazStav (i, k, v, i0, k0); // vytvor ve stromu novy uzel

            top = q; // nyni k zobrazovani pouzivej novy uzel

            p[i][k] = v; // zapamatuj si cestu zpet

            jump (i+2, k+1, v+1, i, k);
            jump (i-2, k+1, v+1, i, k);
            jump (i+2, k-1, v+1, i, k);
            jump (i-2, k-1, v+1, i, k);

            jump (i+1, k+2, v+1, i, k);
            jump (i-1, k+2, v+1, i, k);
            jump (i+1, k-2, v+1, i, k);
            jump (i-1, k-2, v+1, i, k);

            top = save; // navrat na puvodni pozici ve stromu
        }
    }
}

void Jezdec::hraj ()
{
    int x0 = ui->spinBoxX->value ();
    int y0 = ui->spinBoxY->value ();

    limit = ui->horizontalSlider->sliderPosition ();;

    udelej_chybu = ui->checkBox->isChecked ();

    top = nullptr;

    ui->tableWidget->clearContents ();

    ui->treeWidget->clear ();

    for (int i = 0; i < N; i++)
        for (int k = 0; k < N; k++)
            p[i][k] = -1;

    jump (x0, y0, 0, -1, -1);
}

void Jezdec::on_actionRun_triggered()
{
    hraj ();
}

void Jezdec::on_treeWidget_itemClicked (QTreeWidgetItem* treeItem, int column)
{
    MyItem * item = dynamic_cast <MyItem*> (treeItem);
    if (item != nullptr)
    {
        ui->traceTable->clearContents();
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                ui->traceTable->setItem (j, i, item->pole [i][j]->clone());
    }
}

int main(int argc, char *argv[])
{
    QApplication a (argc, argv);

    Jezdec w;
    w.show();

    return a.exec();
}
