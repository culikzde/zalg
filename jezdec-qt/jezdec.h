#ifndef MOJEOKNO_H
#define MOJEOKNO_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QTreeWidgetItem>

namespace Ui {
class Jezdec;
}

class MyItem;

const int N = 8;

class Jezdec : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Jezdec(QWidget *parent = 0);
    ~Jezdec();

    void zobrazStav();

private:
    int limit;
    QTreeWidgetItem * top;
    int p [N][N];
    bool udelej_chybu;

     MyItem * zobrazStav(int x, int y, int n, int x0, int y0);

    void jump (int i, int k, int v, int i0, int k0);
    void hraj ();
    
private slots:
    void on_treeWidget_itemClicked(QTreeWidgetItem* item, int column);
    void on_actionRun_triggered();

private:
    Ui::Jezdec * ui;
};

class MyItem : public QTreeWidgetItem // uzel stromu
{
   public:
       MyItem () : QTreeWidgetItem () { }
       QTableWidgetItem * pole [N][N]; // pamatuje si figurky
};

#endif // MOJEOKNO_H
