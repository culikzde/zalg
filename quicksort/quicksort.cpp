#include <iostream>
#include <stdlib.h> /* srand, rand */
#include <time.h>   /* time */
using namespace std;

const int N = 1000;
// const int N = 1000000;

int a [N];

inline void swap (int & a, int & b)
{
    int t = a;
    a = b;
    b = t;
}

void quicksort (int r, int s)
{
    int h = a[(r + s) / 2]; // r <= (r+s)/2 <= s
    int i = r;
    int k = s;

    while(i <= k)
    {
        while (a[i] < h) i++;
        while (a[k] > h) k--;

        if (i <= k)
        {
            swap(a[i], a[k]);
            i++;
            k--;
        }
    }

    if(r < k) quicksort (r, k);
    if(s > i) quicksort (i, s);
}

int main()
{
    srand (time (NULL));

    for (int i = 0; i < N; i++)
        a[i] = rand() % 50000;


    quicksort (0, N-1);

    /*
    for (int k = N-2; k >= 0; k--)
        for (int i = 0; i <= k; i++)
            if (a[i + 1] < a[i])
                swap (a[i], a[i + 1]);
    */

    const bool print = (N <= 100000);
    bool ok = true;
    for (int i = 0; i < N; i++)
    {
        if (print) cout << a[i];
        if (i < N - 1 && a[i] > a[i + 1]) { ok = false; cout << " CHYBA"; }
        if (print) cout << endl;
    }
    if (ok)  cout << "O.K." << endl;
}

